//
//  ViewController.m
//  ReadPipeDelimitedTextFile
//
//  Created by Edward P. Legaspi on 8/1/14.
//  Copyright (c) 2014 Kalidad Biz Solutions. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
